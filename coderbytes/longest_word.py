"""
Return the longest word in the string
 - If there are multiple return the first one
 - Ignore punctuation
 - Assume sen will not be empty

"""
import string


def LongestWord(sen):
    # code goes here
    sen_clean = sen.translate(str.maketrans("", "", string.punctuation))
    words = sen_clean.split(" ")
    max_len = 0
    for word in words:
        if len(word) > max_len:
            max_len = len(word)
            out_word = word
    return out_word


# keep this function call here
print(LongestWord("I love dogs"))
