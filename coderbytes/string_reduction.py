"""
Get input strings, reduce any characters that don't match to the third one, can only have a,b,c as chars
"""

def StringReduction(strParam):
    # Corner case if it's too short
    if len(strParam) == 1:
        return 1

    letters = ['a', 'b', 'c']

    replaced_elem = True
    while replaced_elem:
        replaced_elem = False
        for i in range(len(strParam) - 1):
            if strParam[i] != strParam[i + 1]:
                replaced_elem = True
                rep_letter = [x for x in letters if x != strParam[i] and x != strParam[i + 1]]
                assert len(rep_letter) == 1, "Should only have a single letter left"
                strParam = strParam[:i] + rep_letter[0] + strParam[i + 2:]
                break

    return len(strParam)


print(StringReduction("cab"))
print(StringReduction("bcab"))
# keep this function call here
# print(StringReduction(input())