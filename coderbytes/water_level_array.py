"""
From test for Insight AI

Question:
 Given array of heights of buildings,
 determine how much water you can capture between them

Can't run a full set of tests, but this looks like it should work
"""
import numpy as np


def determine_water(heights):
    # Not super optimized
    # Fill all non-building with water
    # From top remove all water tiles that spill out of edges
    # Do sum of matrix
    water = np.ones((max(heights), len(heights)))
    for i, height in enumerate(heights):
        water[:height, i] = 0

    # Remove rows that leak past the edges
    for rind in range(water.shape[0]):
        # left leaks
        if water[rind, 0] == 1:
            pos = 0
            while water[rind, pos] == 1 and pos < len(water):
                water[rind, pos] = 0
                pos += 1
        # right leaks
        if water[rind, -1] == 1:
            pos = -1
            while water[rind, pos] == 1 and pos > -1 * len(water):
                water[rind, pos] = 0
                pos -= 1
    return np.sum(water)


if __name__ == "__main__":
    heights = [1, 4, 1, 1, 5, 6, 1, 1, 1, 6]
    print(determine_water(heights))
