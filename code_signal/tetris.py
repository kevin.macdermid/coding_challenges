"""
You are given a matrix of integers field of size height × width representing a game field, and also a matrix of integers figure of size 3 × 3 representing a figure. Both matrices contain only 0s and 1s, where 1 means that the cell is
occupied, and 0 means that the cell is free.
1
1 The actual image is presented to candidates in an animated gif format, which can be viewed here.
6
You choose a position at the top of the game field where you put the figure and then drop it down. The figure
falls down until it either reaches the ground (bottom of the field) or lands on an occupied cell, which blocks it
from falling further. After the figure has stopped falling, some of the rows in the field may become fully occupied.
Your task is to find the dropping position such that at least one full row is formed. As a dropping position, you
should return the column index of the cell in the game field which matches the top left corner of the figure’s 3 × 3
matrix. If there are multiple dropping positions satisfying the condition, feel free to return any of them. If there
are no such dropping positions, return -1.
Note: The figure must be dropped so that its entire 3 × 3 matrix fits inside the field, even if part of the matrix is
empty.
Examples
For
field = [[0, 0, 0],
 [0, 0, 0],
 [0, 0, 0],
 [1, 0, 0],
 [1, 1, 0]]
and
figure = [[0, 0, 1],
 [0, 1, 1],
 [0, 0, 1]]
The output should be solution(field, figure) = 0. As you can see, the field is a 3 x 3 matrix and the figure
can be dropped only from position 0. When the figure stops falling, two fully occupied rows are formed, so dropping position 0 satisfies the condition.

Plan:
- Loop over possible drop positions
- Find drop location
- Check for complete rows
"""

